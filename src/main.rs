/*
 * Copyright © 2021 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

use std::{
    fs::File,
    io::{BufReader, BufWriter, Write},
    str::FromStr,
};

#[macro_use]
extern crate serde_derive;

mod mali;
use mali::*;

const LICENSE_HEADER: &str = r#"<!--
Copyright © 2017-2020 ARM Limited.
Copyright © 2021 Collabora, Ltd.
Author: Antonio Caggiano <antonio.caggiano@collabora.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next
paragraph) shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
"#;

const id_offset: usize = "ARM_Mali-".len();
const id_len: usize = 4;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename = "metrics")]
struct Metrics {
    pub id: String,
    #[serde(rename = "category", default)]
    pub categories: Vec<Category>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename = "category")]
struct Category {
    pub name: String,
    pub per_cpu: String,
    #[serde(rename = "event", default)]
    pub events: Vec<Event>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Event {
    pub offset: Option<usize>,
    pub advanced: Option<String>,
    pub counter: String,
    pub title: String,
    pub name: String,
    pub description: String,
    pub multiplier: Option<u32>,
    pub units: String,
}

fn main() {
    let xml_dir_path = "xml";
    let xml_dir = std::fs::read_dir(xml_dir_path)
        .expect(&format!("Failed to read {} directory", xml_dir_path));

    for xml_file in xml_dir.into_iter().filter(|dir| {
        dir.as_ref()
            .unwrap()
            .file_name()
            .to_str()
            .unwrap()
            .ends_with(".xml")
    }) {
        let xml_entry = xml_file.as_ref().unwrap();
        let xml_name = xml_entry.file_name().into_string().unwrap();
        let xml_path = format!("{}/{}", xml_dir_path, xml_name);

        println!("Processing {}", xml_name);

        let f = File::open(&xml_path).expect("Failed to open xml file");

        let r = BufReader::new(f);

        let mut categories: Vec<Category> = quick_xml::de::from_reader(r).expect("Failed to read");

        let id = String::from(&categories[0].events[0].counter[id_offset..id_offset + id_len]);

        add_offset(&mut categories).expect("Failed to add offset");

        trim_names(&mut categories).expect("Failed to trim names");

        let metrics = Metrics { id, categories };

        let out_dir = format!("{}/out", xml_dir_path);
        if std::fs::read_dir(&out_dir).is_err() {
            std::fs::create_dir(&out_dir)
                .expect(&format!("Failed to create output dir at {}", out_dir));
        }

        let out_path = format!("{}/out/{}", xml_dir_path, &xml_name);
        let fw = File::create(&out_path).unwrap();
        let mut w = BufWriter::new(fw);
        w.write(LICENSE_HEADER.as_bytes())
            .expect("Failed to write license header");
        quick_xml::se::to_writer(w, &metrics).expect("Failed to write");
        println!("\tResults written to {}", out_path);
    }
}

fn trim_names(categories: &mut Vec<Category>) -> Result<(), String> {
    for category in categories.iter_mut() {
        let trimmed = match category.name.strip_prefix("Mali ") {
            Some(t) => t,
            None => return Err(String::from("Failed to strip Mali prefix")),
        };
        category.name = String::from(trimmed);

        for event in category.events.iter_mut() {
            let underscore_len = 1;
            let trimmed = &event.counter[id_offset + id_len + underscore_len..];
            event.counter = String::from(trimmed);

            let trimmed = match event.title.strip_prefix("Mali ") {
                Some(t) => t,
                None => return Err(String::from("Failed to strip Mali prefix")),
            };
            event.title = String::from(trimmed);
        }
    }

    Ok(())
}

fn add_offset(categories: &mut Vec<Category>) -> Result<(), String> {
    for (category_id, category) in categories.iter_mut().enumerate() {
        for event in category.events.iter_mut() {
            let counter_name = &event.counter["ARM_Mali-".len()..];

            let underscore = counter_name.find("_").unwrap();
            let product_name = counter_name[..underscore].to_uppercase();

            if let Ok(product_id) = ProductId::from_str(&product_name) {
                let product = PRODUCTS
                    .iter()
                    .find(|p| p.product_id == product_id)
                    .expect(&format!("Failed to find product with id {:?}", product_id));

                for (mut offset, name) in product.counter_names.iter().enumerate() {
                    if counter_name == *name {
                        if category_id == CounterBlock::Shader as usize {
                            offset += BLOCK_SIZE
                        } else if category_id == CounterBlock::Mmu as usize {
                            offset -= BLOCK_SIZE
                        }
                        event.offset = Some(offset);
                    }
                }
            } else {
                return Err(format!(
                    "Failed to find product id from name {}",
                    product_name
                ));
            }
        }
    }

    Ok(())
}
